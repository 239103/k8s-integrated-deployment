cd /opt/k8s/work
source env.sh
rm -rf deployment coredns-deployment
git clone https://github.com/coredns/deployment.git
mv deployment coredns-deployment
cd /opt/k8s/work/coredns-deployment/kubernetes
./deploy.sh -i ${CLUSTER_DNS_SVC_IP} -d ${CLUSTER_DNS_DOMAIN} | kubectl apply -f -