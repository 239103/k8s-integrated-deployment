#!/bin/bash

kubectl get csr | grep Pending | awk '{print $1}' | xargs kubectl certificate approve