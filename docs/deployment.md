[TOC]
## 项目介绍
- 目标

通过iac工具ansible实现在服务器资源上部署k8s集群

- 项目git

https://gitee.com/239103/k8s-integrated-deployment

## 使用说明
### 准备环境
#### 新建项目目录
mkdir -p /app/tasks
cd /app/tasks

#### 克隆项目
git clone https://gitee.com/239103/k8s-integrated-deployment

#### 生成项目UUID
PROJECT_NAME=`uuidgen`

#### 目录更名
mv k8s-integrated-deployment/ $PROJECT_NAME

#### 设置物理集群环境变量
KVMHOST=kvmhost-51-202
TEMPLATE=centos7
CLUSTER_NAME=dog
NODE1_IP=192.168.51.24
NODE2_IP=192.168.51.25
NODE3_IP=192.168.51.26

#### 生成集群环境变量env.sh
cd $PROJECT_NAME
PROJECT_HOME=`pwd`
ANSIBLE_CONFIG=$PROJECT_HOME/ansible.cfg
sed -i "/PROJECT_NAME=/cPROJECT_NAME=$PROJECT_NAME" $PROJECT_HOME/env.sh
sed -i "/PROJECT_HOME=/cPROJECT_HOME=$PROJECT_HOME" $PROJECT_HOME/env.sh
sed -i "/KVMHOST=/cKVMHOST=$KVMHOST" $PROJECT_HOME/env.sh
sed -i "/TEMPLATE=/cTEMPLATE=$TEMPLATE" $PROJECT_HOME/env.sh
sed -i "/CLUSTER_NAME=/cCLUSTER_NAME=$CLUSTER_NAME" $PROJECT_HOME/env.sh
sed -i "/NODE1_IP=/cNODE1_IP=$NODE1_IP" $PROJECT_HOME/env.sh
sed -i "/NODE2_IP=/cNODE2_IP=$NODE2_IP" $PROJECT_HOME/env.sh
sed -i "/NODE3_IP=/cNODE3_IP=$NODE3_IP" $PROJECT_HOME/env.sh
source env.sh

for i in `seq 1 3`
do
    sed -i "/NODE${i}\_NAME=/cNODE${i}\_NAME=${CLUSTER_NAME}${i}" $PROJECT_HOME/env.sh
done
sed -i "/NODE_IPS=/cNODE_IPS=($NODE1_IP $NODE2_IP $NODE3_IP)" $PROJECT_HOME/env.sh
sed -i "/NODE_NAMES=/cNODE_NAMES=(${CLUSTER_NAME}1 ${CLUSTER_NAME}2 ${CLUSTER_NAME}3)" $PROJECT_HOME/env.sh
sed -i "/ETCD_ENDPOINTS=/cexport ETCD_ENDPOINTS=\"https://${NODE1_IP}:2379,https://${NODE2_IP}:2379,https://${NODE3_IP}:2379\"" $PROJECT_HOME/env.sh
sed -i "/ETCD_NODES/cexport ETCD_NODES=\"${CLUSTER_NAME}1=https://${NODE1_IP}:2380,${CLUSTER_NAME}2=https://${NODE2_IP}:2380,${CLUSTER_NAME}3=https://${NODE3_IP}:2380\"" $PROJECT_HOME/env.sh
sed -i "/BOOTSTRAP_TOKEN=/cBOOTSTRAP_TOKEN=$(head -c 16 /dev/urandom | od -An -t x | tr -d ' ')" $PROJECT_HOME/env.sh
sed -i "/ENCRYPTION_KEY=/cENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)" $PROJECT_HOME/env.sh

### 执行playbook



