PROJECT_HOME=/root/projects/k8s-integrated-deployment/
ANSIBLE_CONFIG=/root/projects/k8s-integrated-deployment/ansible.cfg
KVMHOST=kvmhost-51-202
TEMPLATE=centos7
CLUSTER_NAME=candy
NODE1_NAME=candy1
NODE2_NAME=candy2
NODE3_NAME=candy3
NODE1_IP=192.168.51.21
NODE2_IP=192.168.51.22
NODE3_IP=192.168.51.23
NODE_IPS=(192.168.51.21 192.168.51.22 192.168.51.23)
NODE_NAMES=(candy1 candy2 candy3)
NETMASK=255.255.255.0
GATEWAY=192.168.51.52
DNS1=202.96.128.86
ROOTPASS=sina.com
# 生成 EncryptionConfig 所需的加密 key
export ENCRYPTION_KEY=kSkTiYzZ+Z5FfymVkDsoPlPsDQ/6ih9R0GBEKej94Y4=
# etcd 集群服务地址列表
export ETCD_ENDPOINTS="https://192.168.51.21:2379,https://192.168.51.22:2379,https://192.168.51.23:2379"
# etcd 集群间通信的 IP 和端口
export ETCD_NODES="candy1=https://192.168.51.21:2380,candy2=https://192.168.51.22:2380,candy3=https://192.168.51.23:2380"
# kube-apiserver 的反向代理(kube-nginx)地址端口
export KUBE_APISERVER="https://127.0.0.1:8443"
# 节点间互联网络接口名称
export IFACE="eth0"
# etcd 数据目录
export ETCD_DATA_DIR="/data/k8s/etcd/data"
# etcd WAL 目录，建议是 SSD 磁盘分区，或者和 ETCD_DATA_DIR 不同的磁盘分区
export ETCD_WAL_DIR="/data/k8s/etcd/wal"
# k8s 各组件数据目录
export K8S_DIR="/data/k8s/k8s"
## DOCKER_DIR 和 CONTAINERD_DIR 二选一
# docker 数据目录
export DOCKER_DIR="/data/k8s/docker"
# containerd 数据目录
export CONTAINERD_DIR="/data/k8s/containerd"
## 以下参数一般不需要修改
# TLS Bootstrapping 使用的 Token，可以使用命令 head -c 16 /dev/urandom | od -An -t x | tr -d ' ' 生成
BOOTSTRAP_TOKEN=10c97821d214e4c2f85d0972457d477f
# 最好使用 当前未用的网段 来定义服务网段和 Pod 网段
# 服务网段，部署前路由不可达，部署后集群内路由可达(kube-proxy 保证)
SERVICE_CIDR="10.254.0.0/16"
# Pod 网段，建议 /16 段地址，部署前路由不可达，部署后集群内路由可达(flanneld 保证)
CLUSTER_CIDR="172.30.0.0/16"
# 服务端口范围 (NodePort Range)
export NODE_PORT_RANGE="30000-32767"
# kubernetes 服务 IP (一般是 SERVICE_CIDR 中第一个IP)
export CLUSTER_KUBERNETES_SVC_IP="10.254.0.1"
# 集群 DNS 服务 IP (从 SERVICE_CIDR 中预分配)
export CLUSTER_DNS_SVC_IP="10.254.0.2"
# 集群 DNS 域名（末尾不带点号）
export CLUSTER_DNS_DOMAIN="cluster.local"
# 将二进制目录 /opt/k8s/bin 加到 PATH 中
export PATH=/opt/k8s/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
#指定etcd网络参数所存储键值的key
export FLANNEL_ETCD_PREFIX="/flannel/network"
